package webdemo

import io.javalin.Context
import io.javalin.Javalin
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

fun createSampleData() {
    transaction {
        addLogger(StdOutSqlLogger)

        SchemaUtils.drop(NotesTable) // drop NotesTable if existed
        SchemaUtils.create(NotesTable) // create NotesTable

        // todo: populate with sample data
        Note.new {
            text = "Welcome to Databases!"
            createdAt = DateTime.now()
        }

        Note.new {
            text = "Import all the tables within 2 hours"
            createdAt = DateTime.now()
        }
    }
}

fun indexView(ctx: Context) {
    val entryList = transaction { Note.all().toList() }

    ctx.render("index.ftl", mapOf("items" to entryList))
}

fun addEntry(ctx: Context) {
    // retrieve the user's input (inside a form field called "text")
    val userText = ctx.formParam("text")

    // insert this into the DB
    if (!userText.isNullOrEmpty()) {
        transaction {
            Note.new {
                text = userText
                createdAt = DateTime.now()
            }
        }
    }

    ctx.redirect("/")
}

fun deleteEntry(ctx: Context) {
    val id = ctx.queryParam("id")
            .orEmpty()
            .toIntOrNull()

    if (id != null) {
        transaction {
            Note.findById(id)?.run {
                delete()
            }
        }
    }

    ctx.redirect("/")
}

fun main(args: Array<String>) {
	val app = Javalin.create()
			.start(7000)

    // Connect to the DB
    Database.connect("jdbc:mysql://localhost:3306/dbdemo",
            driver = "com.mysql.cj.jdbc.Driver",
            user = "root",
            password = "abc1234"
    )

    createSampleData()
    app.get("/", ::indexView)
    app.get("/add") { ctx -> ctx.render("newentry.ftl")}
    app.post("/add", ::addEntry)
    app.get("/delete", ::deleteEntry)
}
