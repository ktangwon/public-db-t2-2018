package webdemo

import org.jetbrains.exposed.dao.*

object NotesTable: IntIdTable() {
    // automatic: id

    val text = text("reminder_text")
    val createdAt = datetime("created_at")
}

class Note(id: EntityID<Int>): IntEntity(id) {
    companion object : IntEntityClass<Note>(NotesTable)

    var text by NotesTable.text
    var createdAt by NotesTable.createdAt
}