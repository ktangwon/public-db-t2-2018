<html>
<style>
    body {
        font-family: Helvetica;
    }
    .datetime {
        color: blue;
    }
</style>
<body>
<h1>TODO List</h1>
(<a href="/add">add</a>|<a href=""/>refresh</a>)
<ul>
    <#list items as i>
        <li>${i.text} (<span class="datetime">${i.createdAt}</span>)
                (<a href="/delete?id=${i.id}">delete</a>)
        </li>
    </#list>
</ul>
</body>
</html>
